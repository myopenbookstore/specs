Software specifications

The goal is to describe what to do (what are a bookseller's daily
needs), and say "how" when needed (technical considerations).

We build on:

* the previous specifications: http://abelujo.cc/specs/
* and the software itself: https://gitlab.com/vindarel/abelujo/
